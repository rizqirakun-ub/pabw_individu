<?php

namespace App\Http\Views\Composers;

use Illuminate\View\View;
use App\Models\Page;

class MainComposer
{
    /**
     * The user repository implementation.
     *
     */
    protected $pages;

    /**
     * Create a new profile composer.
     *
     * @return void
     */
    public function __construct()
    {
        // Dependencies are automatically resolved by the service container...
        $this->pages = Page::all();
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('pages', $this->pages);
    }
}
