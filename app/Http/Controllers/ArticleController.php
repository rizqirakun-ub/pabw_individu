<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->articlesManual = [
            999 => [
                'id'        => 999,
                'title'     => 'Manual Article',
                'content'   => 'manual ArticleController'
            ],
            0 => [
                'id'    => 0,
                'title' => 'M Rizqi Ramadhan - 185150400111054',
                'content' => '<a href="https://gitlab.com/rizqirakun/pabw_individu" class="text-indigo">https://gitlab.com/rizqirakun/pabw_individu</a>'
            ]
        ];
    }
    /**
 * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::All();
        foreach ($this->articlesManual as $item) {
            $articles->push((object) $item);
        }

        return view('article', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Article::where('id', $id)->first();
        $manual = $this->articlesManual[$id] ?? null;

        if (empty($data)) {
            if (!empty($manual)) {
                $data = $manual;
            } else {
                return abort(404);
            }
        }
        return view('articleDetail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
