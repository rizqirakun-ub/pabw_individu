<h1>Tugas Individu Pemrograman Aplikasi Berbasis Web</h1>

Nama    : Muhammad Rizqi Ramadhan<br>
NIM     : 185150400111054<br>
Kelas   : SI-A<br>

<h2>Note</h2>

1. Start server<br>
`php artisan serve`<br>

2. Prepare Database<br>
`php artisan migrate`<br>
`php artisan db:seed`<br>

3. Development<br>
`npm install` => single installation <br>
`php artisan serve`<br>
`npm run watch`<br> 

4. Build assets<br>
`npm run build`<br>

<h2>Tools</h2>

- Laravel 8
- PHP 7.4
- MAMP 5.2
- Visual Studio Code
- Git Kraken
- Sequel Pro
