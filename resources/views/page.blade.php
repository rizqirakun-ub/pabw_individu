@extends('layouts.app')

@section('content')
    <h1>{{ $title }}</h1>
    <p>
        {{ $content }}
    </p>
@endsection

@section('scripts')
    
@endsection