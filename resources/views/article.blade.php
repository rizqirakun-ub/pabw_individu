@extends('layouts.app')

@section('content')
    <ul class="list-none mt-10">
        @foreach ($articles as $item)
        <li class="mb-5">
            <a class="btn" href="{{ url("article/{$item->id}") }}">{{ $item->title }}</a>
        </li>
        @endforeach
    </ul>
@endsection

@section('scripts')
    
@endsection