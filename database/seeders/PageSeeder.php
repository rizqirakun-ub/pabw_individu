<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Page;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add 3 page => home, article, about
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $pages = [
            [
                'title' => 'About Me',
                'description' => 'This is my story',
                'content' => 'M Rizqi Ramadhan 185150400111054',
                'created_at' => $now
            ]
        ];
        Page::truncate();
        Page::insert($pages);
    }
}
