<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Article;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // cleaning article
        Article::truncate();
        // generate 10 article from factory faker
        Article::factory(5)->create();
    }
}
